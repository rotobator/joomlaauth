<?php

/**
 * Joomla authentication source configuration parser.
 *
 * Copyright SIL International, Steve Moitozo, <steve_moitozo@sil.org>, http://www.sil.org 
 *
 * This class is a Joomla authentication source which authenticates users
 * against a Joomla site located on the same server.
 *
 *
 * The homepage of this project: http://code.google.com/p/joomlaauth/
 *
 * See the joomlaauth-entry in config-templates/authsources.php for information about
 * configuration of these options.
 *
 * @author Steve Moitozo <steve_moitozo@sil.org>, SIL International
 * @author Antonio Garcia <rotobator@gmail.com>
 * 
 * @package joomlaauth
 * @version $Id$
 */
class sspmod_joomlaauth_ConfigHelper {

    /**
     * String with the location of this configuration.
     * Used for error reporting.
     */
    private $location;

    /**
     * The filesystem path to the Joomla directory
     */
    private $joomlaroot;

    /**
     * Whether debug output is enabled.
     *
     * @var bool
     */
    private $debug;

    /**
     * The attributes we should fetch. Can be NULL in which case we will fetch all attributes.
     */
    private $attributes;

    /**
     * The url to return when logout
     */
    private $return_logout_url;

    /**
     * The Joomla base URL
     */
    private $joomla_base_url;

    /**
     * The Joomla logout URL
     */
    private $joomla_logout_url;

    /**
     * The Joomla login URL
     */
    private $joomla_login_url;

    /**
     * Constructor for this configuration parser.
     *
     * @param array $config  Configuration.
     * @param string $location  The location of this configuration. Used for error reporting.
     */
    public function __construct($config, $location) {
        assert('is_array($config)');
        assert('is_string($location)');

        $this->location = $location;

        /* Parse configuration. */
        $config = SimpleSAML_Configuration::loadFromArray($config, $location);

        $this->joomlaroot = $config->getString('joomlaroot');
        $this->debug = $config->getBoolean('debug', FALSE);
        $this->attributes = $config->getArray('attributes', NULL);
        $this->return_logout_url = $config->getString('return_logout_url', NULL);
        $this->joomla_base_url = $config->getString('joomla_base_url', NULL);
        $this->joomla_logout_url = $config->getString('joomla_logout_url', NULL);
        $this->joomla_login_url = $config->getString('joomla_login_url', NULL);
    }

    /**
     * Return the debug
     *
     * @param boolean $debug whether or not debugging should be turned on
     */
    public function getDebug() {
        return $this->debug;
    }

    /**
     * Return the joomladir
     *
     * @param string $joomlaroot the directory of the Joomla site
     */
    public function getJoomlaroot() {
        return $this->joomlaroot;
    }

    /**
     * Return the attributes
     *
     * @param array $attributes the array of Joomla attributes to use, NULL means use all available attributes
     */
    public function getAttributes() {
        return $this->attributes;
    }

    /**
     * Return this URL after logout
     *
     * @param array return_logout_url the URL to return
     */
    public function getReturnLogoutURL() {
        return $this->return_logout_url;
    }

    /**
     * Return the Joomla base URL
     *
     * @param array $joomla_base_url the URL of the Joomla logout page
     */
    public function getJoomlaBaseURL() {
        return $this->joomla_base_url;
    }

    /**
     * Return the Joomla logout URL
     *
     * @param array $joomla_logout_url the URL of the Joomla logout page
     */
    public function getJoomlaLogoutURL() {
        return $this->joomla_logout_url;
    }

    /**
     * Return the Joomla login URL
     *
     * @param array $joomla_login_url the URL of the Joomla login page
     */
    public function getJoomlaLoginURL() {
        return $this->joomla_login_url;
    }

}
