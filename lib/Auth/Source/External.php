<?php

/**
 * Joomlaath authentication source for using Joomla's login page.
 *
 * Copyright SIL International, Steve Moitozo, <steve_moitozo@sil.org>, http://www.sil.org
 *
 * This class is an authentication source which is designed to
 * more closely integrate with a Joomla site. It causes the user to be
 * delivered to Joomla's login page, if they are not already authenticated.
 *
 *
 * The homepage of this project: http://code.google.com/p/joomlaauth/
 *
 * !!! NOTE WELLL !!!
 *
 * You must configure store.type in config/config.php to be something
 * other than phpsession, or this module will not work. SQL and memcache
 * work just fine. The tell tail sign of the problem is infinite browser
 * redirection when the SimpleSAMLphp login page should be presented.
 *
 *
 * You must install the joomlaauth4ssp module into Joomla to complete the
 * login integration, since this class will send users to the Joomla login
 * page to authenticate instead of presenting a SimpleSAMLphp login page.
 *
 * -------------------------------------------------------------------
 *
 * To use this put something like this into config/authsources.php:
 *
 *  'joomla-userpass' => array(
 *    'joomlaauth:External',
 *
 *    // The filesystem path of the Joomla directory.
 *    'joomlaroot' => '/var/www/joomla',
 *
 *    // Whether to turn on debug
 *    'debug' => true,
 *
 *    // URL to return when logout
 *    'return_logout_url' => 'https://mySP.com/',
 * 
 *    // the base URL of the Joomla. Use '/' when installed in a FQDN site.
 *    'joomla_base_url' => '/joomla',

 *    // the URL of the Joomla logout page
 *    'joomla_logout_url' => 'https://www.example.com/joomla/user/logout',
 *
 *    // the URL of the Joomla login page
 *    'joomla_login_url' => 'https://www.example.com/joomla/user',
 *
 *    // Which attributes should be retrieved from the Joomla site.
 *
 *              'attributes' => array(
 *                                     array('joomlauservar' => 'username', 'callit' => 'uid'),
 *                                     array('joomlauservar' => 'name', 'callit' => 'cn'),
 *                                     array('joomlauservar' => 'email', 'callit' => 'mail'),
 *                                     array('joomlauservar' => 'fullname',  'callit' => 'givenName')
 *                                   ),
 *
 * Format of the 'attributes' array explained:
 *
 * 'attributes' can be an associate array of attribute names, or NULL, in which case
 * all attributes are fetched.
 *
 * If you want everything (except) the password hash do this:
 *    'attributes' => NULL,
 *
 * If you want to pick and choose do it like this:
 * 'attributes' => array(
 *          array('joomlauservar' => 'uid',  'callit' => 'uid),
 *                     array('joomlauservar' => 'name', 'callit' => 'cn'),
 *                     array('joomlauservar' => 'email', 'callit' => 'mail'),
 *                      ),
 *
 *  The value for 'joomlauservar' is the variable name for the attribute in the
 *  Joomla user object.
 *
 *  The value for 'callit' is the name you want the attribute to have when it's
 *  returned after authentication. You can use the same value in both or you can
 *  customize by putting something different in for 'callit'. For an example,
 *  look at the entry for name above.
 *
 *
 * @author Steve Moitozo <steve_moitozo@sil.org>, SIL International
 * @author Antonio Garcia <rotobator@gmail.com>
 * 
 * @package joomlaauth
 * @version $Id$
 */
class sspmod_joomlaauth_Auth_Source_External extends SimpleSAML_Auth_Source {

    /**
     * Whether to turn on debugging
     */
    private $debug;

    /**
     * The Joomla installation directory
     */
    private $joomlaroot;

    /**
     * The Joomla user attributes to use, NULL means use all available
     */
    private $attributes;

    /**
     * The url to return when logout
     */
    private $return_logout_url;
    
    /**
     * The base URL
     */
    private $joomla_base_url;

    /**
     * The logout URL of the Joomla site
     */
    private $joomla_logout_url;

    /**
     * The login URL of the Joomla site
     */
    private $joomla_login_url;

    /**
     * Constructor for this authentication source.
     *
     * @param array $info  Information about this authentication source.
     * @param array $config  Configuration.
     */
    public function __construct($info, $config) {
        assert('is_array($info)');
        assert('is_array($config)');

        /* Call the parent constructor first, as required by the interface. */
        parent::__construct($info, $config);


        /* Get the configuration for this module */
        $joomlaAuthConfig = new sspmod_joomlaauth_ConfigHelper($config, 'Authentication source ' . var_export($this->authId, TRUE));

        $this->debug = $joomlaAuthConfig->getDebug();
        $this->attributes = $joomlaAuthConfig->getAttributes();
        $this->return_logout_url = $joomlaAuthConfig->getReturnLogoutURL();
        $this->joomla_base_url = $joomlaAuthConfig->getJoomlaBaseURL();
        $this->joomla_logout_url = $joomlaAuthConfig->getJoomlaLogoutURL();
        $this->joomla_login_url = $joomlaAuthConfig->getJoomlaLoginURL();

        defined('_JEXEC') || define('_JEXEC', 1);
        defined('DS') || define('DS', '/');
        defined('JOOMLA_ROOT') || define('JOOMLA_ROOT', $joomlaAuthConfig->getJoomlaroot());
        defined('JPATH_BASE') || define('JPATH_BASE', JOOMLA_ROOT);
        
        require_once ( JOOMLA_ROOT . DS . 'configuration.php' );
        require_once ( JOOMLA_ROOT . DS . 'includes' . DS . 'defines.php' );
        require_once ( JOOMLA_ROOT . DS . 'includes' . DS . 'framework.php' );
        require_once ( JOOMLA_ROOT . DS . 'libraries' . DS . 'joomla' . DS . 'factory.php' );
 
    }

    /**
     * Retrieve attributes for the user.
     *
     * @return array|NULL  The user's attributes, or NULL if the user isn't authenticated.
     */
    private function getUser() {


        JFactory::getApplication('site')->initialise();
        $joomlauser = & JFactory::getUser();
        
        // get all the attributes out of the user object
        $userAttrs = get_object_vars($joomlauser);

        // define some variables to use as arrays
        $userAttrNames = null;
        $attributes = null;

        // figure out which attributes to include
        if (NULL == $this->attributes) {
            $userKeys = array_keys($userAttrs);

            // populate the attribute naming array
            foreach ($userKeys as $userKey) {
                $userAttrNames[$userKey] = $userKey;
            }
        } else {
            // populate the array of attribute keys
            // populate the attribute naming array
            foreach ($this->attributes as $confAttr) {
                $userKeys[] = $confAttr['joomlauservar'];
                $userAttrNames[$confAttr['joomlauservar']] = $confAttr['callit'];
            }
        }

        // an array of the keys that should never be included
        // (e.g., pass)
        $skipKeys = array('pass');

        // package up the user attributes
        foreach ($userKeys as $userKey) {

            // skip any keys that should never be included
            if (!in_array($userKey, $skipKeys)) {

                if (is_string($userAttrs[$userKey]) || is_numeric($userAttrs[$userKey]) || is_bool($userAttrs[$userKey])) {

                    $attributes[$userAttrNames[$userKey]] = array($userAttrs[$userKey]);
                } elseif (is_array($userAttrs[$userKey])) {

                    // if the field is a field module field, special handling is required
                    if (substr($userKey, 0, 6) == 'field_') {
                        $attributes[$userAttrNames[$userKey]] = array($userAttrs[$userKey]['und'][0]['safe_value']);
                    } else {
                        // otherwise treat it like a normal array
                        $attributes[$userAttrNames[$userKey]] = $userAttrs[$userKey];
                    }
                }
            }
        }

        return $attributes;
    }

    /**
     * Log in using an external authentication helper.
     *
     * @param array &$state  Information about the current authentication.
     */
    public function authenticate(&$state) {
        assert('is_array($state)');

        $attributes = $this->getUser();
        if ($attributes !== NULL) {
            /*
             * The user is already authenticated.
             *
             * Add the users attributes to the $state-array, and return control
             * to the authentication process.
             */
            $state['Attributes'] = $attributes;
            return;
        }

        /*
         * The user isn't authenticated. We therefore need to
         * send the user to the login page.
         */

        /*
         * First we add the identifier of this authentication source
         * to the state array, so that we know where to resume.
         */
        $state['joomlaauth:AuthID'] = $this->authId;


        /*
         * We need to save the $state-array, so that we can resume the
         * login process after authentication.
         *
         * Note the second parameter to the saveState-function. This is a
         * unique identifier for where the state was saved, and must be used
         * again when we retrieve the state.
         *
         * The reason for it is to prevent
         * attacks where the user takes a $state-array saved in one location
         * and restores it in another location, and thus bypasses steps in
         * the authentication process.
         */
        $stateId = SimpleSAML_Auth_State::saveState($state, 'joomlaauth:External');

        /*
         * Now we generate an URL the user should return to after authentication.
         * We assume that whatever authentication page we send the user to has an
         * option to return the user to a specific page afterwards.
         */
        $returnTo = SimpleSAML_Module::getModuleURL('joomlaauth/resume.php', array(
                    'State' => base64_encode($stateId),
        ));
        /*
         * Get the URL of the authentication page.
         *
         * Here we use the getModuleURL function again, since the authentication page
         * is also part of this module, but in a real example, this would likely be
         * the absolute URL of the login page for the site.
         */
        //			$authPage = $this->joomla_login_url . '?ReturnTo=' . $returnTo;
        $authPage = $this->joomla_login_url;

        /*
         * The redirect to the authentication page.
         *
         * Note the 'ReturnTo' parameter. This must most likely be replaced with
         * the real name of the parameter for the login page.
         * 
         * .htacces has to be configured to redirect:
         * 
         * in .htaccess:
         * RewriteCond %{QUERY_STRING}     ^redir=(.*)$    [NC]
         * RewriteRule ^joomlaauth       ${unescape:%1}  [QSD,NC,R=301,L]
         * 
         * in virtualhost context:
         * RewriteMap unescape int:unescape
         */

	if ( $this->endsWith($this->joomla_base_url, "/" )){
           $returnTo = base64_encode($this->joomla_base_url . "joomlaauth?redir=" . urlencode($returnTo));
	} else {
           $returnTo = base64_encode($this->joomla_base_url . "/joomlaauth?redir=" . urlencode($returnTo));
	}

        SimpleSAML_Utilities::redirect($authPage, array(
            'return' => $returnTo,
        ));

        /*
         * The redirect function never returns, so we never get this far.
         */
        assert('FALSE');
    }

    /**
     * Resume authentication process.
     *
     * This function resumes the authentication process after the user has
     * entered his or her credentials.
     *
     * @param array &$state  The authentication state.
     */
    public static function resume() {

        /*
         * First we need to restore the $state-array. We should have the identifier for
         * it in the 'State' request parameter.
         */
        if (!isset($_REQUEST['State'])) {
            throw new SimpleSAML_Error_BadRequest('Missing "State" parameter.');
        }
        $stateId = base64_decode((string) $_REQUEST['State']);

        /*
         * Once again, note the second parameter to the loadState function. This must
         * match the string we used in the saveState-call above.
         */
        $state = SimpleSAML_Auth_State::loadState($stateId, 'joomlaauth:External');

        /*
         * Now we have the $state-array, and can use it to locate the authentication
         * source.
         */
        $source = SimpleSAML_Auth_Source::getById($state['joomlaauth:AuthID']);
        if ($source === NULL) {
            /*
             * The only way this should fail is if we remove or rename the authentication source
             * while the user is at the login page.
             */
            throw new SimpleSAML_Error_Exception('Could not find authentication source with id ' . $state[self::AUTHID]);
        }

        /*
         * Make sure that we haven't switched the source type while the
         * user was at the authentication page. This can only happen if we
         * change config/authsources.php while an user is logging in.
         */
        if (!($source instanceof self)) {
            throw new SimpleSAML_Error_Exception('Authentication source type changed.');
        }


        /*
         * OK, now we know that our current state is sane. Time to actually log the user in.
         *
         * First we check that the user is acutally logged in, and didn't simply skip the login page.
         */
        $attributes = $source->getUser();
        if ($attributes === NULL) {
            /*
             * The user isn't authenticated.
             *
             * Here we simply throw an exception, but we could also redirect the user back to the
             * login page.
             */
            throw new SimpleSAML_Error_Exception('User not authenticated after login page.');
        }

        /*
         * So, we have a valid user. Time to resume the authentication process where we
         * paused it in the authenticate()-function above.
         */

        $state['Attributes'] = $attributes;
        SimpleSAML_Auth_Source::completeAuth($state);

        /*
         * The completeAuth-function never returns, so we never get this far.
         */
        assert('FALSE');
    }

    /**
     * This function is called when the user start a logout operation, for example
     * by logging out of a SP that supports single logout.
     *
     * @param array &$state  The logout state array.
     */
    public function logout(&$state) {
        assert('is_array($state)');

        if (!session_id()) {
            /* session_start not called before. Do it here. */
            session_start();
        }

        /*
         * In this example we simply remove the 'uid' from the session.
         */
        unset($_SESSION['uid']);

        JFactory::getApplication('site')->initialise();
        $userToken = JSession::getFormToken();

        /*
         * Redirect the user to the Joomla logout page
         */

        if ( $this->endsWith($this->joomla_base_url, "/" )){
	   $returnTo = base64_encode($this->joomla_base_url . "joomlaauth?redir=" . urlencode($this->return_logout_url));
	} else {
	   $returnTo = base64_encode($this->joomla_base_url . "/joomlaauth?redir=" . urlencode($this->return_logout_url));
	}

        header('Location: ' . $this->joomla_logout_url . '&' . $userToken . '=1&return=' . $returnTo);
        die;
    }

    function endsWith($haystack, $needle) {
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
    }

}
