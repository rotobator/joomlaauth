<?php

/**
 * Joomla authentication source for simpleSAMLphp
 *
 * Copyright SIL International, Steve Moitozo, <steve_moitozo@sil.org>, http://www.sil.org 
 *
 * This class is a Joomla authentication source which authenticates users
 * against a Joomla site located on the same server.
 *
 *
 * The homepage of this project: http://code.google.com/p/joomlaauth/
 *
 * !!! NOTE WELLL !!!
 *
 * You must configure store.type in config/config.php to be something
 * other than phpsession, or this module will not work. SQL and memcache
 * work just fine. The tell tail sign of the problem is infinite browser
 * redirection when the SimpleSAMLphp login page should be presented.
 *
 * -------------------------------------------------------------------
 *
 * To use this put something like this into config/authsources.php:
 * 	
 * 	'joomla-userpass' => array(
 * 		'joomlaauth:UserPass',
 * 
 * 		// The filesystem path of the Joomla directory.
 * 		'joomlaroot' => '/var/www/joomla-7.0',
 * 
 * 		// Whether to turn on debug
 * 		'debug' => true,
 * 
 * 		// Which attributes should be retrieved from the Joomla site.				     
 * 				     
 *              'attributes' => array(
 *                                    array('joomlauservar'   => 'uid',  'callit' => 'uid'),
 *                                     array('joomlauservar' => 'name', 'callit' => 'cn'),
 *                                     array('joomlauservar' => 'mail', 'callit' => 'mail'),
 *                                     array('joomlauservar' => 'field_first_name',  'callit' => 'givenName'),
 *                                     array('joomlauservar' => 'field_last_name',   'callit' => 'sn'),
 *                                     array('joomlauservar' => 'field_organization','callit' => 'ou'),
 *                                     array('joomlauservar' => 'roles','callit' => 'roles'),
 *                                   ),
 * 	),
 * 
 * Format of the 'attributes' array explained:
 *
 * 'attributes' can be an associate array of attribute names, or NULL, in which case
 * all attributes are fetched.
 * 
 * If you want everything (except) the password hash do this:
 *  	'attributes' => NULL,
 *
 * If you want to pick and choose do it like this:
 *              'attributes' => array(
 *                                     array('joomlauservar' => 'username', 'callit' => 'uid'),
 *                                     array('joomlauservar' => 'name', 'callit' => 'cn'),
 *                                     array('joomlauservar' => 'email', 'callit' => 'mail'),
 *                                     array('joomlauservar' => 'fullname',  'callit' => 'givenName')
 *                                   ),
 *  The value for 'joomlauservar' is the variable name for the attribute in the 
 *  Joomla user object.
 * 
 *  The value for 'callit' is the name you want the attribute to have when it's
 *  returned after authentication. You can use the same value in both or you can
 *  customize by putting something different in for 'callit'. For an example,
 *  look at the entry for name above.
 *
 *
 * @author Steve Moitozo <steve_moitozo@sil.org>, SIL International
 * @author Antonio Garcia <rotobator@gmail.com>
 * @package joomlaauth
 * @version $Id$
 */
class sspmod_joomlaauth_Auth_Source_UserPass extends sspmod_core_Auth_UserPassBase {

    /**
     * Whether to turn on debugging
     */
    private $debug;

    /**
     * The Joomla installation directory
     */
    private $joomlaroot;

    /**
     * The Joomla user attributes to use, NULL means use all available
     */
    private $attributes;

    /**
     * Constructor for this authentication source.
     *
     * @param array $info  Information about this authentication source.
     * @param array $config  Configuration.
     */
    public function __construct($info, $config) {
        assert('is_array($info)');
        assert('is_array($config)');

        /* Call the parent constructor first, as required by the interface. */
        parent::__construct($info, $config);

        /* Get the configuration for this module */
        $joomlaAuthConfig = new sspmod_joomlaauth_ConfigHelper($config, 'Authentication source ' . var_export($this->authId, TRUE));

        $this->debug = $joomlaAuthConfig->getDebug();
        $this->attributes = $joomlaAuthConfig->getAttributes();

        defined('_JEXEC') || define('_JEXEC', 1);
        defined('DS') || define('DS', '/');
        defined('JOOMLA_ROOT') || define('JOOMLA_ROOT', $joomlaAuthConfig->getJoomlaroot());
        defined('JPATH_BASE') || define('JPATH_BASE', JOOMLA_ROOT);

        require_once ( JOOMLA_ROOT . DS . 'configuration.php' );
        require_once ( JOOMLA_ROOT . DS . 'includes' . DS . 'defines.php' );
        require_once ( JOOMLA_ROOT . DS . 'includes' . DS . 'framework.php' );
        require_once ( JOOMLA_ROOT . DS . 'libraries' . DS . 'joomla' . DS . 'factory.php' );
        require_once ( JOOMLA_ROOT . DS . 'libraries' . DS . 'joomla' . DS . 'user' . DS . 'authentication.php' );
    }

    /**
     * Attempt to log in using the given username and password.
     *
     * On a successful login, this function should return the users attributes. On failure,
     * it should throw an exception. If the error was caused by the user entering the wrong
     * username or password, a SimpleSAML_Error_Error('WRONGUSERPASS') should be thrown.
     *
     * Note that both the username and the password are UTF-8 encoded.
     *
     * @param string $username  The username the user wrote.
     * @param string $password  The password the user wrote.
     * @return array  Associative array with the users attributes.
     */
    protected function login($username, $password) {
        assert('is_string($username)');
        assert('is_string($password)');


        $credentials = array('username' => $username, 'password' => $password);
        $login_site = & JFactory::getApplication('site');
        $login_site->login($credentials, $options = array());

        $authenticate = JAuthentication::getInstance();
        $response = $authenticate->authenticate($credentials, $options = array());

        if ($response->status !== JAuthentication::STATUS_SUCCESS) {
            throw new SimpleSAML_Error_Error('WRONGUSERPASS');
        }

        // get all the attributes out of the user object
        $userAttrs = get_object_vars($response);

        // define some variables to use as arrays
        $userAttrNames = null;
        $attributes = null;

        // figure out which attributes to include
        if (NULL == $this->attributes) {
            $userKeys = array_keys($userAttrs);

            // populate the attribute naming array
            foreach ($userKeys as $userKey) {
                $userAttrNames[$userKey] = $userKey;
            }
        } else {
            // populate the array of attribute keys
            // populate the attribute naming array
            foreach ($this->attributes as $confAttr) {

                $userKeys[] = $confAttr['joomlauservar'];
                $userAttrNames[$confAttr['joomlauservar']] = $confAttr['callit'];
            }
        }

        // an array of the keys that should never be included
        // (e.g., pass)
        $skipKeys = array('pass');

        // package up the user attributes	
        foreach ($userKeys as $userKey) {

            // skip any keys that should never be included
            if (!in_array($userKey, $skipKeys)) {

                if (is_string($userAttrs[$userKey]) || is_numeric($userAttrs[$userKey]) || is_bool($userAttrs[$userKey])) {

                    $attributes[$userAttrNames[$userKey]] = array($userAttrs[$userKey]);
                } elseif (is_array($userAttrs[$userKey])) {

                    // if the field is a field module field, special handling is required
                    if (substr($userKey, 0, 6) == 'field_') {
                        $attributes[$userAttrNames[$userKey]] = array($userAttrs[$userKey]['und'][0]['safe_value']);
                    } else {
                        // otherwise treat it like a normal array
                        $attributes[$userAttrNames[$userKey]] = $userAttrs[$userKey];
                    }
                }
            }
        }

        return $attributes;
    }

}

?>
