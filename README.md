# joomlaauth

This is a fork of the original project located here: https://code.google.com/p/drupalauth/ which implements the same functionality
as drupalauth on Joomla.

The aim of this fork is to provide a drop-in replacement for sites which need
more functionality or flexibility in their IDP.

- - -

## Overview

Joomla + SimpleSAMLphp + joomlaalauth = Complete SAML Identity Provider (IdP)

Users interact with Joomla to create accounts, manage accounts, and authenticate. SAML SPs interact with SimpleSAMLphp. Joomlaauth ties Joomla to SimpleSAMLphp.

The joomlaauth module for simpleSAMLphp makes it easy to create a SAML or Shibboleth identity provider (IdP) by enabling authentication of users against a Joomla site on the same server. This allows the administrator to leverage the user management and integration capabilities of Joomla for managing the identity life cycle.

**NOTE:** This is software establishes a SAML identity provider (IdP) using Joomla as the user database instead of LDAP. If you want to establish your Joomla site as a SAML service provider (SP) connected to a SAML or Shibboleth IdP, see the simplesamlphp_auth module for Joomla.

## Installation

Instructions are available at https://bitbucket.org/rotobator/joomlaauth/wiki/Home

## License

See LICENSE.txt
